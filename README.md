# NDSpp
* Some C++ classes and procedures to make NDS development more bearable.
* I was going to call it NDS++ but the URL is consistent this way.
* Built on top of libnds: https://libnds.devkitpro.org
# Usage
* I have copied the Makefile from the libnds arm9lib example.
* To build the lib:
    * `$ cd ndspp && make`
* To link against it using a libnds Makefile template:
    1. Add `-lndspp` to the LIBS.
    2. Add the base directory of ndspp to LIBDIRS.
    3. (Optional) Configure your linter to find the include files.
