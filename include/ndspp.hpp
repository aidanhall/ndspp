#pragma once

#include <nds.h>
#include <nds/arm9/console.h>
#include <nds/ndstypes.h>

namespace nds
{

    /* a fixed-point number */
    struct fix {
	/* state */
	int32 bits;

	/* initializers */
	fix() {}
	fix(int32 b):
	    bits { b } {}
	fix(float f):
	    bits { floattof32(f) } {}
	fix (const fix& c):
	    bits { c.bits } {}

	/* conversions */
	operator int32() { return f32toint(bits); }
	operator float() { return f32tofloat(bits); }

	/* methods */
	static fix sqrt(const fix& f);
    };

    fix operator*(const fix& a, const fix& b);
    fix operator/(const fix& num, const fix& den);
    fix operator+(const fix& a, const fix& b);
    fix operator-(const fix& a, const fix& b);
}
