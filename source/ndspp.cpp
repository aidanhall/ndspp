#include "ndspp.hpp"

namespace nds {
    /* fix */
    inline fix fix::sqrt(const fix& f) {
	return sqrtf32(f.bits);
    }

    inline fix operator*(const fix& a, const fix& b) {
	return mulf32(a.bits, b.bits);
    }
    inline fix operator/(const fix& num, const fix& den) {
	return divf32(num.bits, den.bits);
    }
    inline fix operator+(const fix& a, const fix& b) {
	return a.bits + b.bits;
    }
    inline fix operator-(const fix& a, const fix& b) {
	return a.bits - b.bits;
    }
};
